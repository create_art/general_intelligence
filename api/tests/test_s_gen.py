from gic.problems.big_small import SequenceChallenge

def test_num_sequence():
    seq = SequenceChallenge()
    assert seq.get_sequence()