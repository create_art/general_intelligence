from flask import Flask
import pytest
from gic.app import create_app

@pytest.fixture()
def app() -> Flask:
    app = create_app()
    yield app

@pytest.fixture()
def client(app):
    return app.test_client()