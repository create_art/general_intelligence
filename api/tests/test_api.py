
def test_default(client):
    response = client.get("/")
    assert response.status_code == 200

def test_rotation(client):
    response = client.get("/get_rotation")
    assert response.status_code == 200

def test_sequence(client):
    response = client.get("/get_sequence")
    assert response.status_code == 200

def test_comparison(client):
    response = client.get("/get_comparison")
    assert response.status_code == 200

def test_odd_one(client):
    response = client.get("/get_odd_one")
    assert response.status_code == 200

def test_case_sensitive(client):
    response = client.get("/get_case")
    assert response.status_code == 200