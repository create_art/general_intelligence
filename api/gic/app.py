from flask import Flask
from gic.challenge_factory import r_gen,s_gen

def create_app() -> Flask:
    app = Flask(__name__)

    @app.route("/")
    def get_default():
        return "This is a test of your intelligence"


    @app.route("/get_rotation")
    def get_rotation_challenge():
        return "This should be an image of 4 R's"

    @app.route("/get_comparison")
    def get_comparison_challenge():
        return "Alpha Go is smarter than Lee Sedol"

    @app.route("/get_sequence")
    def get_sequence_challenge():
        return s_gen()

    return app


app = create_app()

if __name__ == "__main__":
    app.run()