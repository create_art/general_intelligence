from typing import Tuple
from PIL import Image
from gic.problems.mind_spin import RotationChallenge
from gic.problems.big_small import SequenceChallenge

def r_gen(top_r : Tuple[int,int], bottom_r : Tuple[int,int]) -> Image:
    challenge = RotationChallenge(top_char=top_r,bottom_char=bottom_r)
    return challenge.get_image()

def s_gen() -> Tuple[int,int,int]:
    challenge = SequenceChallenge()
    return challenge.get_sequence()
