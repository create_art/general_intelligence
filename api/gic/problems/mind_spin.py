from dataclasses import dataclass
from typing import Tuple
from PIL import Image

@dataclass
class RotationChallenge():
    """Class for storing mental rotation challenge data"""
    top_char : Tuple[int,int]
    bottom_char : Tuple[int,int]
    character : str = "R"

    def get_image(self):
        """Create 4 xR's in 2 rows to mirror/rotate standard R typography. Uses Pillow
        to perform the image rendering.

        Args:
            top_r (Tuple[int,int]): First integer hint x4 rotations 0,90,180,270 degrees,
            second integer hints mirroring
            bottom_r (Tuple[int,int]): Same as above for lower character

        Returns:
            Image: Images in grey, white and black containing the transformed sets of "R"
        """
        # Create canvas to place the R's
        im = Image.new(mode="RGB", size=(800,450), color=(127,127,127))
        # Create 2 vertical rectangles to hold letters
        # Create transformed R's as appropriate
        # Paste R's on vertical rectangles
        # Paste vertical rectangles on overall canvas
        return im
