
from dataclasses import dataclass
from typing import Tuple
from random import randint

@dataclass
class SequenceChallenge:
    """This class encapsulates the sequence challenge and its response
    It also functions as a factory for new sequences
    """
    sequence : Tuple[int,int,int]  = (1,7,3)     # Actual 3 number sequence
    furthest_index : int  = 2                    # Solution to the 3 number sequence challenge, index of number in tuple
    s_range : Tuple[int,int]  = (1,30)           # Lower and upper range in which 3 number sequence lies

    def get_sequence(self) -> Tuple[int,int,int]:
        self.sequence = (
            randint(self.s_range[0],self.s_range[1]),
            randint(self.s_range[0],self.s_range[1]),
            randint(self.s_range[0],self.s_range[1]))
        min_s = min(min(self.sequence[0],self.sequence[1]),self.sequence[2])
        max_s = max(max(self.sequence[0],self.sequence[1]),self.sequence[2])
        for n in self.sequence:
            diff_min = abs(n - min_s)
        for m in self.sequence:
            diff_max = abs(n - max_s)
        # TODO: Store index of number with max difference
        return self.sequence

